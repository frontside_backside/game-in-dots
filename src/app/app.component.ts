import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  settingsSubscription: Subscription;
  winnersSubscription: Subscription;
  timeout = 2000;
  gridSize = 2;
  userScore = 0;
  machineScore = 0;
  dots = [];
  settings = [];
  winners = [];
  winner = null;
  currentDotIndex = null;
  timer = null;
  userName: string = 'player1';
  startBtnEnabled = false;
  startBtnText = 'Play';

  constructor(private http: HttpClient, private _snackBar: MatSnackBar) {}

  ngOnInit() {
    this.settingsSubscription = this.http.get<any>(`/game-settings`).subscribe( (data) => {
      this.settings = data;
    });
    this.winnersSubscription = this.http.get<any>('/winners').subscribe( (data) => this.winners = data.reverse());
  }

  ngOnDestroy() {
    if (this.settingsSubscription) this.settingsSubscription.unsubscribe();
    if (this.winnersSubscription) this.winnersSubscription.unsubscribe();
  }

  changeGridSettings(settingsIndex: number) {
    if (settingsIndex === -1) return;
    this.gridSize = this.settings[settingsIndex].field;
    this.timeout = this.settings[settingsIndex].delay;
    
    this.dots = [];
    for (let index = 0; index < (this.gridSize * this.gridSize); index++) {
      this.dots.push({
        status: 'new'
      })
    }
    this.startBtnEnabled = true;
  }

  startGame() {
    this.clearGrid();
    this.startBtnEnabled = false;
   
    this._snackBar.open('Ready..', null, {
      duration: 900,
    });
    setTimeout(() => {
      this._snackBar.open('Steady..', null, {
        duration: 900,
      });
    }, 1000);
    setTimeout(() => {
      this._snackBar.open('GO!', null, {
        duration: 900,
      });
    }, 2000);
    
    setTimeout(() => {
      //todo: close sneakbar
      this.setNewActiveDot();
    }, 3000);
  }

  setNewActiveDot () {
    if (this.checkIfGameOver()) {
      this.endGame();
      this.startBtnEnabled = true;
      this.startBtnText = 'Play again';
      return;
    };
    this.currentDotIndex = this.getNewDotIndex();
    this.dots[this.currentDotIndex].status = 'waiting'; 
    this.startTimer(this.currentDotIndex);
  }

  clearGrid() {
    this.winner = null;
    this.userScore = 0;
    this.machineScore = 0;
    this.dots.forEach( el => el.status = 'new');
    this.currentDotIndex = null;
  }

  getNewDotIndex(): number {
    const rand =  Math.round(Math.random() * Math.pow(this.gridSize, 2));
    if (this.dots[rand] && this.dots[rand].status === 'new') {
      return rand;
    } else {
      return this.getNewDotIndex();
    }
  }

  selectDot(index: number) {
    if (index === this.currentDotIndex) {
      clearTimeout(this.timer);
      this.dots[index].status = 'user';
      this.userScore++;
      this.setNewActiveDot();
    } 
  }

  startTimer(index: number) {
    this.timer = setTimeout(() => {
      this.dots[index].status = 'machine';
      this.machineScore++;
      this.setNewActiveDot();
    }, this.timeout);
  }

  checkIfGameOver(): boolean {
    return ( this.machineScore > (Math.pow(this.gridSize, 2)/2) || this.userScore > (Math.pow(this.gridSize, 2)/2) || this.dots.filter( (el) => el.status === "new").length === 0 ) ? true : false;
  }

  endGame() {
    if (this.userScore === this.machineScore) {
      return;
    }
    const winner = (this.userScore > this.machineScore)? this.userName : 'computer AI';
    this.winner = winner;
    this.http.post<any>('/winners', {winner: winner, date: new Date()})
    .subscribe( (data) => {
      this.winners = data.reverse();
    });
  }

  
}
