import { Injectable } from '@angular/core';
import { HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { delay, mergeMap, materialize, dematerialize } from 'rxjs/operators';

// import { User } from '@/_models';

@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        const gameSettings: {field: number, delay: number}[] = [
            {field: 5, delay: 1200},
            {field: 7, delay: 1100},
            {field: 9, delay: 1000}
        ];


        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {

            if (request.url.endsWith('/game-settings') && request.method === 'GET') {
                return ok(gameSettings);
            }

            if (request.url.endsWith('/winners') && request.method === 'GET') {
                let winners: {winner: string, date: any}[];
                if (localStorage.getItem('winners')) {
                    winners = JSON.parse(localStorage.getItem('winners') );
                } else {
                    winners = [];
                }
                return ok(winners);
            }

            if (request.url.endsWith('/winners') && request.method === 'POST') {
                let winners: {winner: string, date: any}[];
                if (localStorage.getItem('winners')) {
                    winners = JSON.parse(localStorage.getItem('winners') );
                } else {
                    winners = [];
                }
                winners.push({winner: request.body.winner, date: request.body.date});
                localStorage.setItem('winners', JSON.stringify(winners));
                return ok(winners);
            }

            // pass through any requests not handled above
            return next.handle(request);
        }))
        // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
        .pipe(materialize())
        .pipe(delay(500))
        .pipe(dematerialize());

        // private helper functions

        function ok(body) {
            return of(new HttpResponse({ status: 200, body }));
        }

        function error(message) {
            return throwError({ status: 400, error: { message } });
        }
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};